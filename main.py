from fastapi import FastAPI
from pydantic import BaseModel
from joblib import load

app = FastAPI()

# Load the saved models and StandardScaler
sc = load('standard_scaler.joblib')
knn = load('knn.joblib')
rf = load('rf.joblib')
svc = load('svc.joblib')
lr = load('lr.joblib')

# Create a class to handle request body
class Iris(BaseModel):
    sepal_length: float 
    sepal_width: float 
    petal_length: float 
    petal_width: float 

# Create endpoint for KNN model
@app.post('/predict/knn')
def predict_knn(iris: Iris):
    data = [[
        iris.sepal_length,
        iris.sepal_width,
        iris.petal_length,
        iris.petal_width,
    ]]
    data_std = sc.transform(data)
    prediction = knn.predict(data_std)
    return {'prediction': int(prediction[0])}

# Similarly for Random Forest, SVM and Logistic Regression
@app.post('/predict/rf')
def predict_rf(iris: Iris):
    data = [[
        iris.sepal_length,
        iris.sepal_width,
        iris.petal_length,
        iris.petal_width,
    ]]
    data_std = sc.transform(data)
    prediction = rf.predict(data_std)
    return {'prediction': int(prediction[0])}

@app.post('/predict/svc')
def predict_svc(iris: Iris):
    data = [[
        iris.sepal_length,
        iris.sepal_width,
        iris.petal_length,
        iris.petal_width,
    ]]
    data_std = sc.transform(data)
    prediction = svc.predict(data_std)
    return {'prediction': int(prediction[0])}

@app.post('/predict/lr')
def predict_lr(iris: Iris):
    data = [[
        iris.sepal_length,
        iris.sepal_width,
        iris.petal_length,
        iris.petal_width,
    ]]
    data_std = sc.transform(data)
    prediction = lr.predict(data_std)
    return {'prediction': int(prediction[0])}
