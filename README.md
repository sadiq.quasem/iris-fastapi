# Iris Classification API

This project contains a FastAPI application that provides endpoints for predicting the species of iris flowers using four different machine learning models: K-Nearest Neighbors (KNN), Random Forest (RF), Support Vector Machine (SVM), and Logistic Regression (LR).

## Requirements

This project requires Python 3.7+ and the following Python packages:

- `scikit-learn`
- `fastapi`
- `uvicorn`
- `joblib`

You can install these packages using pip:

```bash
pip install scikit-learn fastapi uvicorn joblib
```

## Training the Models

The machine learning models are trained on the iris dataset from `sklearn.datasets`. The training script is `train_models.py`. This script trains the models and saves them, along with the StandardScaler used to standardize the features, as joblib files.

To train the models and save them, simply run the script:

```bash
python train_models.py
```

## Running the API

The FastAPI application is contained in `main.py`. This application loads the trained models and provides four endpoints for making predictions:

- `/predict/knn` for KNN
- `/predict/rf` for RF
- `/predict/svc` for SVM
- `/predict/lr` for LR

To start the application, use the following command:

```bash
uvicorn main:app --reload
```

This will start the application with hot-reloading enabled on `localhost` port `8000`.

## Making Predictions

To make a prediction, send a POST request to one of the prediction endpoints with a JSON body containing the iris features. The features should be in the following format:

```json
{
    "sepal_length": 5.1,
    "sepal_width": 3.5,
    "petal_length": 1.4,
    "petal_width": 0.2
}
```

Here's an example using `curl`:

```bash
curl -X POST "http://localhost:8000/predict/knn" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"sepal_length\":5.1,\"sepal_width\":3.5,\"petal_length\":1.4,\"petal_width\":0.2}"
```

The response will be a JSON object containing the prediction:

```json
{
    "prediction": 0
}
```

## License

This project is open source and available under the [MIT License](LICENSE).

---

This README provides instructions on how to install the requirements, train the models, run the FastAPI application, and make predictions. You can modify it as necessary to fit your project structure and requirements. If your project is hosted on GitHub, you can also add a link to the license file in the License section.