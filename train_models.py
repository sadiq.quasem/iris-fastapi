from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
import joblib

# Load iris dataset
iris = datasets.load_iris()
X = iris.data
y = iris.target

# Split into train and test sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Standardize features
sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

# Save the standardizer
joblib.dump(sc, 'standard_scaler.joblib')

# Train KNN model
knn = KNeighborsClassifier(n_neighbors=3)
knn.fit(X_train, y_train)

# Compute and print accuracy on test set
knn_pred = knn.predict(X_test)
knn_accuracy = accuracy_score(y_test, knn_pred)
print(f'KNN accuracy: {knn_accuracy}')

# Save the model
joblib.dump(knn, 'knn.joblib')

# Train RF model
rf = RandomForestClassifier(random_state=42)
rf.fit(X_train, y_train)

# Compute and print accuracy on test set
rf_pred = rf.predict(X_test)
rf_accuracy = accuracy_score(y_test, rf_pred)
print(f'RF accuracy: {rf_accuracy}')

# Save the model
joblib.dump(rf, 'rf.joblib')

# Train SVM model
svc = SVC(random_state=42)
svc.fit(X_train, y_train)

# Compute and print accuracy on test set
svc_pred = svc.predict(X_test)
svc_accuracy = accuracy_score(y_test, svc_pred)
print(f'SVM accuracy: {svc_accuracy}')

# Save the model
joblib.dump(svc, 'svc.joblib')

# Train LR model
lr = LogisticRegression(random_state=42)
lr.fit(X_train, y_train)

# Compute and print accuracy on test set
lr_pred = lr.predict(X_test)
lr_accuracy = accuracy_score(y_test, lr_pred)
print(f'LR accuracy: {lr_accuracy}')

# Save the model
joblib.dump(lr, 'lr.joblib')
